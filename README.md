# SimpleOBIS

SimpleOBIS is an example written in R that allows you to create an image(occplot.png) of the occurrences present in OBIS starting from a scientific name and a polygon.

As can be seen, there are 2 scripts, one to be integrated with the SAI simpleobisSAI.R and the other with the CCP simpleobisCCP.R.
The differences are few between the two scripts only that the CPP takes the parameters directly from the command line.

Starting from this example, you can first create a docker image in which it is installed the file simpleobisCCP.R.

The image created in this way can be executed in a container with the following command:

```
$ docker run -i -t --rm --name simpleobis-cont simpleobis /bin/sh -c '/bin/mkdir /ccp_data ;/usr/bin/Rscript --vanilla simpleobisCCP.R "Sarda sarda" "POLYGON((-5.657713322217344 45.88175260533238,38.99072417778265 45.88175260533238,38.99072417778265 29.60135866020714,-5.657713322217344 29.60135866020714,-5.657713322217344 45.88175260533238))"'


```
You can also run it directly from the container shell:

```
$ docker run -i -t --rm --name simpleobis-cont simpleobis sh

# mkdir /ccp_data
# Rscript --vanilla simpleobisCCP.R "Sarda sarda" "POLYGON((-5.657713322217344 45.88175260533238,38.99072417778265 45.88175260533238,38.99072417778265 29.60135866020714,-5.657713322217344 29.60135866020714,-5.657713322217344 45.88175260533238))"

```

Note in the examples above we added the /ccp_app folder to test the created image locally. In reality this is created by the CCP service and therefore in production there is no need to do it so the command would be like this for integration:

```
/bin/sh -c '/usr/bin/Rscript --vanilla simpleobisCCP.R "{{sname}}" "{{usedPolygon}}"'
```


To be able to create an image from this application you need to have Docker and Docker-Compose installed on your machine and the relative python packages, see: 
[Docker](https://docs.docker.com/engine/),
[Docker-Compose](https://docs.docker.com/compose/install/) and

## Useful Commands

### Create Docker Image 
```
docker build -t simpleobis .
```

### Save Docker Image in file
```
docker save simpleobis | gzip > simpleobis.tar.gz
```

### Publish Docker Image on DockerHub
Re-tagging an existing local image: 

```
docker tag simpleobis <hub-user>/<repo-name>[:<tag>]
```

Login in DockerHub(use your Docker ID):

```
docker login
```

Now you can push this repository to the registry designated by its name or tag:

```
docker push <hub-user>/<repo-name>:<tag>
```

Then logout for security:

```
docker logout
```


## Authors

* **Giancarlo Panichi** ([ORCID](http://orcid.org/0000-0001-8375-6644)) - [ISTI-CNR Infrascience Group](http://nemis.isti.cnr.it/groups/infrascience)



## License

This project is licensed under the EUPL V.1.1 License - see the [LICENSE.md](LICENSE.md) file for details.


## About the gCube Framework
This software is part of the [gCubeFramework](https://www.gcube-system.org/ "gCubeFramework"): an
open-source software toolkit used for building and operating Hybrid Data
Infrastructures enabling the dynamic deployment of Virtual Research Environments
by favouring the realisation of reuse oriented policies.
 
The projects leading to this software have received funding from a series of European Union programmes see [FUNDING.md](FUNDING.md)

