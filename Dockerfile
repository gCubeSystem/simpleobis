# Dockerfile
#
# Author: Giancarlo Panichi
###############################################################################

# Base
#FROM rhub/r-minimal:4.2.3
FROM r-base:4.2.3

#r-base
RUN apt-get update
RUN apt-get install apt-utils
RUN apt-get install -y curl 
RUN apt-get install -y libcurl4-openssl-dev
RUN apt-get install libpng++-dev
RUN apt-get install libpng-dev
RUN apt-get install libxml2
RUN apt-get install libxml2-dev
RUN apt-get install fontconfig
RUN apt-get install libfontconfig
RUN apt-get install -y r-cran-rgdal 
RUN apt-get install -y r-cran-systemfonts
RUN apt-get install -y r-cran-base64enc
RUN apt-get install -y r-cran-ggplot2
RUN apt-get install -y r-cran-httr
RUN apt-get install -y r-cran-httpuv
RUN apt-get install -y r-cran-fs
RUN apt-get install -y r-cran-sf
RUN apt-get install -y r-cran-raster
RUN apt-get install -y r-cran-maps
RUN apt-get install -y libgeos-dev

RUN install2.r --error --skipinstalled rgeos 
RUN install2.r --error --skipinstalled terra 
RUN install2.r --error --skipinstalled robis 

#Clean
RUN apt-get clean && rm -rf /var/lib/apt/lists/* && rm -fr /tmp/*

# copy R script
COPY simpleobisCCP.R /
